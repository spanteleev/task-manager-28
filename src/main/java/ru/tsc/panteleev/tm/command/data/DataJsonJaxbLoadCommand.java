package ru.tsc.panteleev.tm.command.data;

import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.Domain;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJsonJaxbLoadCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from json file";

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD FROM JSON FILE]");
        System.setProperty(JAVAX_XML_BIND_CONTEXT_FACTORY, ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY);
        @NotNull final File file = new File(FILE_JAXB_JSON);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, APPLICATION_JSON);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

}
