package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.model.AbstractUserOwnedModel;

import java.util.*;
import java.util.stream.Collectors;

public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) return Collections.emptyList();
        return models.stream()
                .filter(model -> userId.equals(model.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        return findAll(userId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @NotNull final Sort sort) {
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public M findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        return models.stream()
                .filter(model -> id.equals(model.getId()) && userId.equals(model.getUserId()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findById(userId, id);
        return model == null ? null : remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @Nullable final M model = findByIndex(userId, index);
        return model == null ? null : remove(model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findById(userId, id) != null;
    }

    @Override
    public long getSize(@Nullable final String userId) {
        return models.stream()
                .filter(model -> userId.equals(model.getUserId()))
                .count();
    }
}
